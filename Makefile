up:
	docker-compose up -d
migrate:
	docker-compose run --rm app php bin/console doctrine:migrations:migrate
composer-install:
	docker run --rm -v ./:/app composer composer i
clear:
	docker-compose stop
	docker-compose run --rm -u root app rm -rf logs/*
	docker-compose run --rm -u root app rm -rf vendor
	docker-compose run --rm -u root app rm -rf data/*
	docker-compose run --rm -u root app rm -rf var/*
	docker-compose down --volumes --remove-orphans --rmi all
start-ws:
	docker run --rm -v ./:/var/www/html/ --name ws --env-file .env -u 1000:1000 --network=bot-assistant_low_code_link -p 8088:8088 plce php bin/console websocket:server
start-debug-ws:
	docker-compose run --rm -p 8088:8088 --name ws-debug app php -dxdebug.mode=debug -dxdebug.client_port=9003 -dxdebug.client_host=172.17.0.1 bin/console websocket:server
stop-ws:
	docker stop -t 2 ws-debug
