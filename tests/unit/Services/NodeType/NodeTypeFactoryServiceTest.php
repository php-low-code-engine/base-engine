<?php

namespace App\Tests\unit\Services\NodeType;

use App\Services\NodeType\NodeTypeFactoryService;
use App\Tests\unit\Services\ExecuteService\TestNodeType;

class NodeTypeFactoryServiceTest extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
    }

    public function testGetNodeTypes()
    {
        $container = self::$kernel->getContainer();
        /** @var NodeTypeFactoryService $nodeTypeService */
        $nodeTypeService = $container->get(NodeTypeFactoryService::class);

        $types = $nodeTypeService->getNodeTypes();

        $this->assertContains(TestNodeType::class, $types);
    }
}
