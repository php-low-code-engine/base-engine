<?php

namespace App\Tests\unit\Services\StoreService;

use App\Services\Store\StoreInterface;
use App\Services\Store\StoreService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StoreServiceTest extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{
    static private ContainerInterface $container;

    const KEY1 = 'mydata1';

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();
    }

    protected function tearDown(): void
    {
        $this->getStore()->remove(self::KEY1);
    }

    private function getStore():StoreInterface
    {
        /** @var StoreInterface $storeService */
        $storeService = self::$container->get(\App\Services\Store\StoreInterface::class);
        return $storeService;
    }

    public function testSetGet()
    {
        $storeService = $this->getStore();

        $this->assertFalse($storeService->has(self::KEY1));
        $storeService->set(self::KEY1, 'string');
        $this->assertTrue($storeService->has(self::KEY1));

        $data = ['data' => 'some'];
        $storeService->set(self::KEY1, $data);
        $this->assertEquals($data, $storeService->get(self::KEY1));

        $storeService->remove(self::KEY1);
        $this->assertFalse($storeService->has(self::KEY1));
    }
}
