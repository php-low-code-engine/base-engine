<?php

namespace App\Tests\unit\Services\ExecuteService;

use App\Entity\Node;
use App\Services\Execution\ExecutionNodeInterface;
use App\Services\Execution\NodesCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NodeCollectionTest extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{
    static private ExecutionFixture $fixture;
    static private ContainerInterface $container;

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();

        $entityManager = static::$container
            ->get('doctrine')
            ->getManager();

        // Load fixtures
        static::$fixture = new ExecutionFixture();
        static::$fixture->load($entityManager);
    }

    public static function tearDownAfterClass(): void
    {
        // UnLoad fixtures
        $entityManager = static::$container
            ->get('doctrine')
            ->getManager();

        static::$fixture->unload($entityManager);
    }

    public function testNextNode()
    {
        $entityManager = static::$container
            ->get('doctrine')
            ->getManager();

        $nodeRepository = $entityManager->getRepository(Node::class);
        /** @var NodesCollection $executionNodes */
        $executionNodes = $nodeRepository->getNodesByWorkflowId(static::$fixture->workflow->getId());

        $nextNode = $executionNodes->findNextNode(static::$fixture->node->getId(), ExecutionNodeInterface::DEFAULT_RESULT);

        $this->assertNotEmpty($nextNode);
    }
}
