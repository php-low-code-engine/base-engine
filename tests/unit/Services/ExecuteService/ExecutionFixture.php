<?php

namespace App\Tests\unit\Services\ExecuteService;

use App\Entity\Node;
use App\Entity\Workflow;
use App\Services\Execution\ExecutionNodeInterface;
use Doctrine\Persistence\ObjectManager;

class ExecutionFixture extends \Doctrine\Bundle\FixturesBundle\Fixture
{

    public Workflow $workflow;
    public Workflow $workflowLoop;
    public Node $node;
    public Node $nodeLoop;
    public Node $execution;
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $workflow = new Workflow();
        $workflow->setName('Test workflow');
        $workflow->setActive(true);
        $workflow->avalable_loop_repeat = 1;
        $manager->persist($workflow);
        $manager->flush();

        $node = new Node();
        $node->setType(TestNodeType::class);
        $node->setName('Test node');
        $node->setWorkflow($workflow);
        $node->setWorkflowId($workflow->getId());
        $manager->persist($node);
        $manager->flush();

        $nodeChild = new Node();
        $nodeChild->setType(TestNodeType::class);
        $nodeChild->setName('Test node 2');
        $nodeChild->setWorkflow($workflow);
        $nodeChild->setWorkflowId($workflow->getId());
        $nodeChild->setParentId($node->getId());
        $nodeChild->setParentStatusresult(ExecutionNodeInterface::DEFAULT_RESULT);
        $manager->persist($nodeChild);
        $manager->flush();

        $this->node = $node;
        $this->workflow = $workflow;

        $this->workflowLoop = new Workflow();
        $this->workflowLoop->setName('Test workflow loop');
        $this->workflowLoop->setActive(true);
        $this->workflowLoop->avalable_loop_repeat = 10;
        $manager->persist($this->workflowLoop);
        $manager->flush();

        $this->nodeLoop = new Node();
        $this->nodeLoop->setType(TestNodeType::class);
        $this->nodeLoop->setName('Test node loop');
        $this->nodeLoop->setWorkflow($this->workflowLoop);
        $this->nodeLoop->setWorkflowId($this->workflowLoop->getId());

        /** @see \App\Tests\unit\Services\ExecuteService\TestNodeType::run */
        $this->nodeLoop->setParentStatusresult('2');

        $manager->persist($this->nodeLoop);
        $manager->flush();
        $this->nodeLoop->setParentId($this->nodeLoop->getId());
        $manager->flush();
    }

    public function unload(ObjectManager $manager)
    {
        $workflow = $manager->getRepository(Workflow::class)->find($this->workflow->getId());
        $manager->remove($workflow);

        $workflowLoop = $manager->getRepository(Workflow::class)->find($this->workflowLoop->getId());
        $manager->remove($workflowLoop);

        $manager->flush();
        $manager->clear();
    }
}
