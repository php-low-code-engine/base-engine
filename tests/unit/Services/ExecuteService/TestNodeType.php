<?php

namespace App\Tests\unit\Services\ExecuteService;

use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Services\Execution\ExecutionData;
use App\Services\Execution\ExecutionNodeInterface;
use App\Services\NodeType\Types\NodeTypeInterface;
use App\Services\Trigger\TriggerableNodeInterface;
use App\Validator\Constraints\NodeParams;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class TestNodeType implements NodeTypeInterface, ExecutionNodeInterface, TriggerableNodeInterface
{

    static public function getName(): string
    {
        return 'Test node type';
    }

    public function configureFields(Node $node): iterable
    {
        return [];
    }

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context)
    {
        // TODO: Implement validateParams() method.
    }

    public function run(NodeExecution $nodeExecution): string
    {
        $nodeExecution->getInputData();
        if ($nodeExecution->getInputData()->get('runned')) {
            $nodeExecution->setOutputData(['runned' => '222']);
            return self::DEFAULT_RESULT;
        } else {
            $nodeExecution->setOutputData(['runned' => '111']);
            return self::DEFAULT_RESULT;
        }
    }

    public function addTrigger(Node $node, string $type)
    {
        // TODO: Implement addTrigger() method.
    }

    public function removeTrigger(Node $node, string $type)
    {
        // TODO: Implement removeTrigger() method.
    }
}
