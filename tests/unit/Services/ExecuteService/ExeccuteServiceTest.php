<?php

namespace App\Tests\unit\Services\ExecuteService;

use App\Entity\Execution;
use App\Entity\NodeExecution;
use App\Services\Execution\ExecutionException;
use App\Services\Execution\ExecutionService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExeccuteServiceTest extends KernelTestCase
{
    static private ExecutionFixture $fixture;
    static private ContainerInterface $container;

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
        static::$container = static::$kernel->getContainer();
    }

    protected function setUp(): void
    {
        $entityManager = static::$container
            ->get('doctrine')
            ->getManager();

        // Load fixtures
        static::$fixture = new ExecutionFixture();
        static::$fixture->load($entityManager);
    }

    protected function tearDown(): void
    {
        $entityManager = static::$container
            ->get('doctrine')
            ->getManager();

        static::$fixture->unload($entityManager);
        $this->getExecutionService()->reset();
    }

    /**
     * @param $class
     * @return ExecutionService
     */
    protected function getExecutionService(): object
    {
        return self::$container->get(\App\Services\Execution\ExecutionService::class);
    }

    public function testStartExecution()
    {
        $entityManager = static::$container
            ->get('doctrine')
            ->getManager();

        $execution = $this->getExecutionService()->createExecution(
            static::$fixture->node->getWorkflow(), static::$fixture->node, ['inputKey' => 'inputValue']);

        $this->getExecutionService()->runExecution($execution);

        $this->assertNotEmpty($execution->getStarted());
        $this->assertNotEmpty($execution->getFinished());
        $this->assertNotEmpty($execution->getUserId());

        /** @var NodeExecution $nodeExecution */
        $nodeExecution = $execution->getNodeExecutions()->first();
        $this->assertEquals('111', $nodeExecution->getOutputData()->get('runned'));

        /** @var NodeExecution $nodeExecution2 */
        $nodeExecution2 = $execution->getNodeExecutions()->last();
        $this->assertEquals('222', $nodeExecution2->getOutputData()->get('runned'));

        $execution_id = $execution->getId();
        /** @var Execution $execution */
        if (!$execution = $entityManager->getRepository(Execution::class)->find($execution_id)) {
            throw new ExecutionException("Not found execution_id by id '{$execution_id}'");
        }

        $this->getExecutionService()->runExecution($execution);
    }

    public function testLooping()
    {
        $this->expectException(ExecutionException::class);

        $executionLoop = $this->getExecutionService()->createExecution(
            static::$fixture->workflowLoop,
            static::$fixture->nodeLoop,
            /** @see \App\Tests\unit\Services\ExecuteService\TestNodeType::run */
            ['runned' => 'inputValueLoop']
        );

        $executionService = $this->getExecutionService();
        $executionService->runExecution($executionLoop);
    }

    /**
     * Only for send message to execution queue
     * But need disable unload fixture
     * @group  excluded
     */
    public function to_testStartExecutionAsync()
    {
        $executionAsync = $this->getExecutionService()->createExecution(
            static::$fixture->node->getWorkflow(), static::$fixture->node, ['inputKey' => 'inputValueAsync']);

        $executionService = $this->getExecutionService();
        $executionService->reset();
        $executionService->runExecutionAsync($executionAsync);

        $this->assertTrue(true);
    }
}
