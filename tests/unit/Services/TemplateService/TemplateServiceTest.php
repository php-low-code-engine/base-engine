<?php

namespace App\Tests\unit\Services\TemplateService;

use App\Entity\Execution;
use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Services\Execution\ExecutionData;
use App\Services\Template\TemplateService;
use App\Tests\unit\Services\ExecuteService\ExecutionFixture;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TemplateServiceTest extends KernelTestCase
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testRender()
    {
        $nodeExecution = new NodeExecution(new Node());

        $data = [
            'te' => [
                'de' => '11'
            ],
            'tste' => [
                'df' => '222'
            ],
            'frffr' => [
                'df' => '333'
            ],
            'ss' => '4444'
        ];

        /** @var TemplateService $service */
        $service = self::$kernel->getContainer()->get(TemplateService::class);

        $result = $service->render('dfrgsdfs {{te.de }} == {{tste.df}}  == {{ frffr.df}}
        dfvdf
        df df {{ ss }}', new ExecutionData($data, $nodeExecution));

        $this->assertEquals('dfrgsdfs 11 == 222  == 333
        dfvdf
        df df 4444', $result);

        $result = $service->render('',  new ExecutionData([
            "update_id" => 71887702,
            "message" => [
            ]
        ], $nodeExecution));
        $this->assertEquals('', $result);

    }

    public function testRenderGlobalContext()
    {
        /** @var TemplateService $service */
        $service = self::$kernel->getContainer()->get(TemplateService::class);

        $execution = new Execution();
        $prevNodeExecution = new NodeExecution(new Node());
        $prevNodeExecution->setInputData([
            'message' => [
                'chat' => [
                    'id' => '234345435345'
                ]
            ]
        ]);
        $prevNodeExecution->setNodeId('630');
        $execution->setNodeExecutions(new ArrayCollection([$prevNodeExecution]));

        $nodeExecution = new NodeExecution(new Node());
        $nodeExecution->setExecution($execution);

        $result = $service->render('{{execution.630.input.message.chat.id}}',  new ExecutionData([], $nodeExecution));
        $this->assertEquals('234345435345', $result);


    }
}
