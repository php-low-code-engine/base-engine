<?php

namespace App\Entity;

use App\Repository\ContextRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'context')]
#[ORM\Entity(repositoryClass: ContextRepository::class)]
class Context
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy:"SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName:"context_id_seq", initialValue:1, allocationSize:1)]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(name: 'name', type: 'text', nullable: false)]
    private ?string $name = null;

    #[ORM\Column(name: 'filter_type', type: 'text', nullable: false)]
    private ?string $filter_type = null;

    #[ORM\Column(name: 'filter', type: 'text', nullable: false)]
    private ?string $filter = null;

    #[ORM\Column(name: 'context', type: 'json', nullable: false)]
    private ?array $context = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(string $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getFilterType(): ?string
    {
        return $this->filter_type;
    }

    public function setFilterType(string $filter_type): static
    {
        $this->filter_type = $filter_type;

        return $this;
    }

    public function getFilter(): ?string
    {
        return $this->filter;
    }

    public function setFilter(string $filter): static
    {
        $this->filter = $filter;

        return $this;
    }

    public function getContext(): ?array
    {
        return $this->context;
    }

    public function setContext(?array $context): static
    {
        $this->context = $context;

        return $this;
    }
}

