<?php

namespace App\Entity;

use App\Repository\WorkflowRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Asserts;

#[ORM\Entity(repositoryClass: WorkflowRepository::class)]
class Workflow
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'SEQUENCE')]
    #[ORM\SequenceGenerator(sequenceName:"workflow_id_seq", initialValue:1, allocationSize:1)]
    #[ORM\Column(type:'integer')]
    #[Groups(['log'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['log'])]
    private ?string $name = null;

    #[ORM\Column]
    #[Groups(['log'])]
    private bool $active = false;

    #[ORM\Column(options: ['default' => '1'])]
    #[Groups(['log'])]
    #[Asserts\NotNull]
    public ?int $avalable_loop_repeat;

    #[ORM\OneToMany(mappedBy: 'workflow', targetEntity: Node::class, cascade: ['remove'], fetch: 'LAZY')]
    #[AppAssert\HasTriggerWorkflow]
    private Collection $nodes;

    #[ORM\OneToMany(mappedBy: 'workflow', targetEntity: Execution::class, cascade: ['remove'], fetch: 'LAZY')]
    private Collection $executions;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;

        return $this;
    }

    public function getCountExecutions():int
    {
        return $this->executions->count();
    }

    /**
     * @return Collection
     */
    public function getNodes(): Collection
    {
        return $this->nodes;
    }

    /**
     * @return Collection
     */
    public function getExecutions(): Collection
    {
        return $this->executions;
    }

    /**
     * @param Collection $nodes
     */
    public function setNodes(Collection $nodes): void
    {
        $this->nodes = $nodes;
    }

    /**
     * @param Collection $executions
     */
    public function setExecutions(Collection $executions): void
    {
        $this->executions = $executions;
    }
}
