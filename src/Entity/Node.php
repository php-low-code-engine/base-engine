<?php

namespace App\Entity;

use App\Repository\NodeRepository;
use App\Services\Execution\ExecutionNodeInterface;
use App\Services\NodeType\NodeTypeFactoryService;
use App\Services\NodeType\Types\NodeTypeInterface;
use App\Services\Trigger\TriggerableNodeInterface;
use App\Validator\Constraints as AppAsserts;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

#[ORM\Entity(repositoryClass: NodeRepository::class)]
#[ORM\Index(columns: ['workflow_id'], name: 'idx_node_workflow_id')]
#[ORM\Index(columns: ['parent_id'], name: 'idx_node_parent_id')]
#[ORM\Index(columns: ['parent_status_result'], name: 'idx_node_parent_status_result')]
#[ORM\Index(columns: ['triggerable'], name: 'idx_node_triggerable')]
#[ORM\UniqueConstraint(columns: ['parent_id', 'parent_status_result'])]
#[UniqueEntity(
    fields: ['triggerable', 'workflow_id'],
    message: 'constraints.messages.only_one_trigger_available',
    /** @see \App\Repository\NodeRepository::getTriggerableNodesByFields */
    repositoryMethod: 'getTriggerableNodesByFields'
)]
class Node
{
    private ?NodeTypeInterface $typeObject = null;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'SEQUENCE')]
    #[ORM\Column]
    #[Groups(['log'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['log'])]
    private ?string $name = null;

    #[ORM\Column]
    #[Groups(['log'])]
    private ?int $workflow_id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['log'])]
    private ?string $type = null;

    #[ORM\Column(nullable: true)]
    #[AppAsserts\NodeParams]
    #[Groups(['log'])]
    private ?array $params = [];

    #[ORM\Column(nullable: true)]
    #[Groups(['log'])]
    private ?int $parent_id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['log'])]
    private ?string $parent_status_result = null;

    #[ORM\Column(nullable: false)]
    #[Groups(['log'])]
    private bool $triggerable = false;

    #[ORM\ManyToOne(targetEntity: Workflow::class, inversedBy: 'nodes')]
    #[ORM\JoinColumn(name: 'workflow_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    private Workflow $workflow;

    #[ORM\OneToMany(mappedBy: 'node', targetEntity: NodeExecution::class, cascade: ['remove'], fetch: 'LAZY')]
    private Collection $nodeExecutions;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function __set($key, $value)
    {
        $methodName = 'set' . $key;
        if (method_exists($this, $methodName)) {
            $this->$methodName($value);
            return;
        }

        $this->params[$key] = $value;
    }

    public function __get($name)
    {
        return $this->params[$name] ?? null;
    }

    public function getTypeObject(NodeTypeFactoryService $nodeTypeService): NodeTypeInterface|TriggerableNodeInterface|ExecutionNodeInterface
    {
        if (!$this->typeObject) {
            $typeObject = $nodeTypeService->getNodeType($this->getType());
            $this->typeObject = $typeObject;
        }

        return $this->typeObject;
    }

    /**
     * @param NodeTypeInterface|null $typeObject
     */
    public function setTypeObject(NodeTypeInterface $typeObject): void
    {
        $this->typeObject = $typeObject;
    }

    public function getParams(): ?array
    {
        return $this->params;
    }

    public function getParam($key)
    {
        return $this->params[$key] ?? null;
    }

    public function setParams(?array $params): static
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWorkflowId(): ?int
    {
        return $this->workflow_id;
    }

    /**
     * @param int|null $workflow_id
     */
    public function setWorkflowId(?int $workflow_id): void
    {
        $this->workflow_id = $workflow_id;
    }

    /**
     * @return Workflow
     */
    public function getWorkflow(): Workflow
    {
        return $this->workflow;
    }

    /**
     * @param Workflow $workflow
     */
    public function setWorkflow(Workflow $workflow): void
    {
        $this->workflow_id = $workflow->getId();
        $this->workflow = $workflow;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    /**
     * @param int|null $parent_id
     */
    public function setParentId(?int $parent_id): void
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return int|null
     */
    public function getParentStatusresult(): ?string
    {
        return $this->parent_status_result;
    }

    /**
     * @param int|null $parent_status_result
     */
    public function setParentStatusresult(?string $parent_status_result): void
    {
        $this->parent_status_result = $parent_status_result;
    }

    /**
     * @return bool
     */
    public function getTriggerable(): bool
    {
        return $this->triggerable;
    }

    /**
     * @param bool $triggerable
     */
    public function setTriggerable(bool $triggerable): void
    {
        $this->triggerable = $triggerable;
    }

    /**
     * @return Collection
     */
    public function getNodeExecutions(): Collection
    {
        return $this->nodeExecutions;
    }

    /**
     * @param Collection $nodeExecutions
     */
    public function setNodeExecutions(Collection $nodeExecutions): void
    {
        $this->nodeExecutions = $nodeExecutions;
    }

}
