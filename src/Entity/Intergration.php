<?php

namespace App\Entity;

use App\Repository\IntergrationRepository;
use App\Services\Provider\ProviderInterface;
use App\Services\Provider\ProviderService;
use App\Validator\Constraints as AppAsserts;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: IntergrationRepository::class)]
class Intergration
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'SEQUENCE')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $type = null;

    #[ORM\Column(nullable: true)]
    #[AppAsserts\ProviderCreds]
    private ?array $creds = null;

    #[ORM\GeneratedValue(strategy: 'SEQUENCE')]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?\DateTimeInterface $created = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $updated = null;

    private ?ProviderInterface $provider = null;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function __set($key, $value)
    {
        $this->creds[$key] = $value;
    }

    public function __get($name)
    {
        return $this->creds[$name] ?? null;
    }

    public function getProvider(ProviderService $providerService): ProviderInterface
    {
        if (!$this->provider) {
            $this->provider = $providerService->getProvider($this->type);
        }
        return $this->provider;
    }

    public function getCreds(): ?array
    {
        return $this->creds;
    }

    public function setCreds(?array $creds): static
    {
        $this->creds = $creds;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): static
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): static
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

}
