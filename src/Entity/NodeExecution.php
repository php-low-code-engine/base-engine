<?php

namespace App\Entity;

use App\Repository\NodeExecutionRepository;
use App\Services\Execution\ExecutionData;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: NodeExecutionRepository::class)]
#[ORM\Index(columns: ['execution_id'], name: 'idx_node_execution_execution_id')]
#[ORM\Index(columns: ['node_id'], name: 'idx_node_execution_node_id')]
#[ORM\Index(columns: ['started'], name: 'idx_node_execution_started')]
#[ORM\Index(columns: ['status_result'], name: 'idx_node_execution_status_result')]
class NodeExecution
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Groups(['log', 'status'])]
    private ?string $id = null;

    #[ORM\Column(type: 'uuid', nullable: false)]
    #[Groups(['log', 'status'])]
    private Uuid $execution_id;

    #[ORM\Column(nullable: false)]
    #[Groups(['log', 'status'])]
    private int $node_id;

    #[ORM\Column(nullable: true)]
    #[Groups(['log', 'status'])]
    private ?array $input_data = [];

    #[ORM\Column(nullable: true)]
    #[Groups(['log', 'status'])]
    private ?array $output_data = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['log', 'status'])]
    private ?\DateTimeInterface $started = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['log', 'status'])]
    private ?\DateTimeInterface $finished = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['log', 'status'])]
    private ?string $status_result = null;

    #[ORM\Column(nullable: false)]
    #[Groups(['log', 'status'])]
    private int $execute_order;

    #[ORM\ManyToOne(targetEntity: Execution::class, cascade: ['persist'], fetch: 'LAZY', inversedBy: 'nodeExecutions')]
    #[ORM\JoinColumn(name: 'execution_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    private Execution $execution;

    #[ORM\ManyToOne(targetEntity: Node::class, cascade: ['persist'], fetch: 'LAZY', inversedBy: 'nodeExecutions')]
    #[ORM\JoinColumn(name: 'node_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    private Node $node;

    public function __construct(Node $node, array $input_data = [])
    {
        $this->node = $node;
        $this->input_data = $input_data;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getExecutionId(): ?Uuid
    {
        return $this->execution_id;
    }

    public function setExecutionId(Uuid $execution_id): static
    {
        $this->execution_id = $execution_id;

        return $this;
    }

    public function getNodeId(): ?int
    {
        return $this->node_id;
    }

    public function setNodeId(int $node_id): static
    {
        $this->node_id = $node_id;

        return $this;
    }

    public function getInputData(): ExecutionData
    {
        return new ExecutionData($this->input_data ?? [], $this);
    }

    public function getOutputData(): ExecutionData
    {
        return new ExecutionData($this->output_data ?? [], $this);
    }

    public function setOutputData(array $output_data): static
    {
        $this->output_data = $output_data;

        return $this;
    }

    public function setInputData(array $input_data): static
    {
        $this->input_data = $input_data;

        return $this;
    }

    public function getStarted(): ?\DateTimeInterface
    {
        return $this->started;
    }

    public function setStarted(?\DateTimeInterface $started): static
    {
        $this->started = $started;

        return $this;
    }

    public function getFinished(): ?\DateTimeInterface
    {
        return $this->finished;
    }

    public function setFinished(?\DateTimeInterface $finished): static
    {
        $this->finished = $finished;

        return $this;
    }

    public function getStatusResult(): ?string
    {
        return $this->status_result;
    }

    public function setStatusResult(?string $status_result): static
    {
        $this->status_result = $status_result;

        return $this;
    }

    /**
     * @param Execution $execution
     */
    public function setExecution(Execution $execution): self
    {
        $this->execution = $execution;
        return $this;
    }

    /**
     * @return Execution
     */
    public function getExecution(): Execution
    {
        return $this->execution;
    }

    /**
     * @return Node
     */
    public function getNode(): Node
    {
        return $this->node;
    }

    /**
     * @param Node $node
     */
    public function setNode(Node $node): void
    {
        $this->node_id = $node->getId();
        $this->node = $node;
    }

    /**
     * @return int
     */
    public function getExecuteOrder(): int
    {
        return $this->execute_order;
    }

    /**
     * @param int $execute_order
     */
    public function setExecuteOrder(int $execute_order): void
    {
        $this->execute_order = $execute_order;
    }

}
