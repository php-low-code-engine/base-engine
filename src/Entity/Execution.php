<?php

namespace App\Entity;

use App\Repository\ExecutionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ExecutionRepository::class)]
#[ORM\Index(columns: ['workflow_id'], name: 'idx_execution_workflow_id')]
#[ORM\Index(columns: ['user_id'], name: 'idx_execution_user_id')]
#[ORM\Index(columns: ['started'], name: 'idx_execution_started')]
#[ORM\Index(columns: ['status'], name: 'idx_execution_status')]
class Execution
{

    const STATUS_STARTED = 'started';
    const STATUS_FINISHED = 'finished';
    const STATUS_STOPPED = 'stopped';
    const STATUS_ERROR = 'error';

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Groups(['log', 'status'])]
    private ?string $id = null;

    #[ORM\Column]
    #[Groups(['log', 'status'])]
    private ?int $workflow_id = null;

    #[ORM\Column]
    #[Groups(['log', 'status'])]
    private ?int $user_id = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['log', 'status'])]
    private ?string $session_id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['log', 'status'])]
    private ?\DateTimeInterface $started = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['log', 'status'])]
    private ?\DateTimeInterface $finished = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['log', 'status'])]
    private ?string $status = null;

    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['merge'], inversedBy: 'executions')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    public ?User $user = null;

    #[ORM\ManyToOne(targetEntity: Workflow::class, cascade: ['persist'], inversedBy: 'executions')]
    #[ORM\JoinColumn(name: 'workflow_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    private Workflow $workflow;

    #[ORM\OneToMany(mappedBy: 'execution', targetEntity: NodeExecution::class, cascade: ['remove'], fetch: 'LAZY')]
    #[ORM\OrderBy(['execute_order' => 'ASC'])]
    private Collection $nodeExecutions;

    public function __construct()
    {
        $this->nodeExecutions = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getWorkflowId(): ?int
    {
        return $this->workflow_id;
    }

    public function setWorkflowId(int $workflow_id): static
    {
        $this->workflow_id = $workflow_id;

        return $this;
    }

    public function getStarted(): ?\DateTimeInterface
    {
        return $this->started;
    }

    public function setStarted(\DateTimeInterface $started): static
    {
        $this->started = $started;

        return $this;
    }

    public function getFinished(): ?\DateTimeInterface
    {
        return $this->finished;
    }

    public function setFinished(?\DateTimeInterface $finished): static
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    /**
     * @param int|null $user_id
     */
    public function setUserId(?int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @param Workflow $workflow
     * @return Execution
     */
    public function setWorkflow(Workflow $workflow): Execution
    {
        $this->workflow_id = $workflow->getId();
        $this->workflow = $workflow;
        return $this;
    }

    /**
     * @param User|null $user
     * @return Execution
     */
    public function setUser(?User $user): Execution
    {
        $this->user_id = $user->getId();
        $this->user = $user;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return Collection
     */
    public function getNodeExecutions(): Collection
    {
        return $this->nodeExecutions;
    }

    /**
     * @return Workflow
     */
    public function getWorkflow(): Workflow
    {
        return $this->workflow;
    }

    /**
     * @param Collection $nodeExecutions
     */
    public function setNodeExecutions(Collection $nodeExecutions): void
    {
        $this->nodeExecutions = $nodeExecutions;
    }

    /**
     * @return string|null
     */
    public function getSessionId(): ?string
    {
        return $this->session_id;
    }

    /**
     * @param string|null $session_id
     */
    public function setSessionId(string $session_id): void
    {
        $this->session_id = $session_id;
    }
}
