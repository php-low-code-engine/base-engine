<?php

namespace App\Controller;

use App\Entity\Execution;
use App\Entity\NodeExecution;
use App\Services\Trigger\Event\HttpRequestTriggerEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class HttpRequestController extends AbstractController
{
    #[Route('/%http_trigger_base_folder%/trigger/{url}', name: 'app_http_request_trigger')]
    public function trigger(string $url, Request $request, EventDispatcherInterface $eventDispatcher): Response
    {
        $httpRequestEvent = new HttpRequestTriggerEvent($url, $request);
        $eventDispatcher->dispatch($httpRequestEvent, 'http.request.event');

        return $httpRequestEvent->getResponse();
    }

    #[Route('/%http_trigger_base_folder%/execution-status/{executionId}', name: 'app_http_request_status')]
    public function nodeExecutionStatus(
        string $executionId,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    ): Response
    {
        $execution = $entityManager->find(Execution::class, $executionId);

        if (!$execution) {
            return new Response($serializer->serialize([
                'error' => 'Not found execution',
                'executionId' => $executionId
            ], 'json'));
        }

        $dataExecution = $serializer->normalize($execution, 'json', ['groups' => 'status']);

        /** @var NodeExecution $lastNodeExecution */
        $lastNodeExecution = $execution->getNodeExecutions()->last();
        $outputData = $serializer->normalize($lastNodeExecution->getOutputData(), 'json', ['groups' => 'status']);

        return new Response($serializer->serialize([
            'dataExecution' => $dataExecution,
            'outputData' => $outputData,
        ], 'json'));
    }
}
