<?php

namespace App\Controller\Admin;

use App\Entity\Variable;
use App\Entity\Workflow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_ADMIN')]
class VariableCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Variable::class;
    }

    protected function getRepository($class): ServiceEntityRepository
    {
        return $this->container->get('doctrine')->getRepository($class);
    }

    private function getWorkflowChoices():array
    {
        $repository = $this->getRepository(Workflow::class);
        $queryBuilder = $repository->createQueryBuilder('wf');

        $wfChoices = [];
        foreach ($queryBuilder->getQuery()->getResult() as $wf) {
            $wfChoices[$wf->getName()] = $wf->getId();
        }

        return $wfChoices;
    }

    public function configureFields(string $pageName): iterable
    {
        yield ChoiceField::new('workflow_id')->setChoices($this->getWorkflowChoices());
        yield TextField::new('name');
        yield TextField::new('value');
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('name')
//            ->add(ArrayFilter::new('workflow_id')->setChoices($this->getWorkflowChoices()));
            ->add('workflow_id');
    }
}
