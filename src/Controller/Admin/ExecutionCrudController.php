<?php

namespace App\Controller\Admin;

use App\Entity\Execution;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_ADMIN')]
class ExecutionCrudController extends AbstractCrudController
{
    private $workflow_id;

    public static function getEntityFqcn(): string
    {
        return Execution::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions =  parent::configureActions($actions);
        $actions->remove(Crud::PAGE_INDEX, Action::NEW);

        return $actions;
    }

    public function configureCrud(Crud $crud): Crud
    {
        if (!$this->workflow_id = $this->container->get('request_stack')->getMainRequest()->get('workflow_id')) {
            throw new BadRequestHttpException('Not found required param "workflow_id"');
        }
        return parent::configureCrud($crud);
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $queryBuilder = parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $queryBuilder->andWhere('entity.workflow_id = :param_workflow')
            ->setParameter('param_workflow', $this->workflow_id);

        return $queryBuilder;
    }
}
