<?php

namespace App\Controller\Admin;

use App\Entity\Intergration;
use App\Services\Provider\ProviderService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_ADMIN')]
class IntergrationCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly ProviderService $providerService
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return Intergration::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name');

        if (Crud::PAGE_NEW === $pageName) {
            $choices = [];
            foreach ($this->providerService->getProviders() as $class) {
                $choices[$class::getName()] = $class;
            }

            yield ChoiceField::new('type')->setChoices($choices);

        } elseif (Crud::PAGE_EDIT === $pageName) {

            /** @var Intergration $integration */
            $integration = $this->getContext()->getEntity()->getInstance();
            yield from $integration->getProvider($this->providerService)->configureFields($integration);
        }

        return parent::configureFields($pageName);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->showEntityActionsInlined();
    }
}
