<?php

namespace App\Controller\Admin;

use App\Entity\Context;
use App\Entity\Execution;
use App\Entity\Intergration;
use App\Entity\Node;
use App\Entity\Session;
use App\Entity\User;
use App\Entity\Variable;
use App\Entity\Workflow;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[IsGranted('ROLE_ADMIN')]
class DashboardController extends AbstractDashboardController
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'dashboard_controller_filepath' => (new \ReflectionClass(static::class))->getFileName(),
        ]);

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle($this->translator->trans('title'));
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard($this->translator->trans('menu.dashboard.title'), 'fa fa-home');
        yield MenuItem::linkToCrud($this->translator->trans('Users'), 'fa-solid fa-users', User::class);
        yield MenuItem::linkToCrud($this->translator->trans('Integrations'), 'fa-solid fa-plug', Intergration::class);
        yield MenuItem::linkToCrud($this->translator->trans('Workflows'), 'fa-solid fa-diagram-project', Workflow::class);
        yield MenuItem::linkToCrud($this->translator->trans('Variables'), 'fa-brands fa-mixer', Variable::class);
        yield MenuItem::linkToCrud($this->translator->trans('Sessions'), 'fa-solid fa-comment-dots', Session::class);
    }
}
