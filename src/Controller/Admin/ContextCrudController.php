<?php

namespace App\Controller\Admin;

use App\Entity\Context;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_ADMIN')]
class ContextCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Context::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name', 'Name');
        yield TextField::new('filter_type','Filter type');
        yield CodeEditorField::new('filter','Filter');
        yield ArrayField::new('context', 'Context');
    }

}
