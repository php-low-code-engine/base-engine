<?php

namespace App\Controller\Admin;

use App\Entity\Workflow;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_ADMIN')]
class WorkflowCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Workflow::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name');
        yield NumberField::new('avalable_loop_repeat');

        if ($pageName === Crud::PAGE_INDEX) {
            yield BooleanField::new('active');
            yield Field::new('CountExecutions')
                ->setTemplateName('crud/field/generic')->formatValue(function ($value, $workflow) {
                    $href = $this->container->get(AdminUrlGenerator::class)
                    ->includeReferrer()
                    ->setRoute('admin', [
                        'entity' => 'execution'
                    ])
                    ->set('workflow_id', $workflow->getId())
                    ->setController(ExecutionCrudController::class)
                    ->generateUrl();
                    return "<a href='{$href}'>{$value}</a>";
            });
        }
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)->showEntityActionsInlined();
    }

    public function configureActions(Actions $actions): Actions
    {
        $container = $this->container;

        $nodesAction = Action::new('nodes_list', 'Edit nodes')
            ->linkToUrl(function (Workflow $workflow) use ($container) {
                 return $container->get(AdminUrlGenerator::class)
                     ->includeReferrer()
                     ->setRoute('admin', [
                         'entity' => 'node'
                     ])
                     ->set('workflow_id', $workflow->getId())
                     ->setController(NodeCrudController::class)
                     ->generateUrl();
            });

        return parent::configureActions($actions)
            ->add(Crud::PAGE_INDEX, $nodesAction);
    }
}
