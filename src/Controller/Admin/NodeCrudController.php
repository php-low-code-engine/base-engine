<?php

namespace App\Controller\Admin;

use App\Entity\Node;
use App\Entity\Workflow;
use App\Services\NodeType\NodeTypeFactoryService;
use App\Services\Trigger\TriggerableNodeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[IsGranted('ROLE_ADMIN')]
class NodeCrudController extends AbstractCrudController
{
    private $workflow_id;
    private TranslatorInterface $translator;

    public function __construct(
        TranslatorInterface $translator,
        private readonly NodeTypeFactoryService $nodeTypeService
    )
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Node::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        if (!$this->workflow_id = $this->container->get('request_stack')->getMainRequest()->get('workflow_id')) {
            throw new BadRequestHttpException('Not found required param "workflow_id"');
        }

        $workflow = $this->getRepository(Workflow::class)->find($this->workflow_id);

        $pageTtitle = $this->translator->trans('node_list.title', ['%workflow_name%' => $workflow->getName()]);

        return parent::configureCrud($crud)
            ->showEntityActionsInlined()
            ->setPageTitle('index', $pageTtitle);
    }

    protected function getRedirectResponseAfterSave(AdminContext $context, string $action): RedirectResponse
    {
        if ($action === Action::NEW) {
            $url = $this->container->get(AdminUrlGenerator::class)
                ->setAction(Action::EDIT)
                ->setEntityId($context->getEntity()->getPrimaryKeyValue())
                ->generateUrl();

            return $this->redirect($url);
        }

        return parent::getRedirectResponseAfterSave($context, $action);
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
            return $action->linkToUrl(function () {
                return $this->container->get(AdminUrlGenerator::class)
                    ->includeReferrer()
                    ->setAction(Action::NEW)
                    ->set('workflow_id', $this->workflow_id)
                    ->setController(NodeCrudController::class)
                    ->generateUrl();
            });
        });

        $actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
            return $action->linkToUrl(function (Node $node) {
                return $this->container->get(AdminUrlGenerator::class)
                    ->includeReferrer()
                    ->setEntityId($node->getId())
                    ->setAction(Action::EDIT)
                    ->set('workflow_id', $this->workflow_id)
                    ->setController(NodeCrudController::class)
                    ->generateUrl();
            });
        });

        return parent::configureActions($actions);
    }

    protected function getRepository($class): ServiceEntityRepository
    {
        return $this->container->get('doctrine')->getRepository($class);
    }

    public function configureFields(string $pageName): iterable
    {
        if (in_array($pageName, [Crud::PAGE_NEW, Crud::PAGE_EDIT])) {
            yield HiddenField::new('workflow_id')->setValue($this->workflow_id);
        }

        yield TextField::new('name');

        if (in_array($pageName, [Crud::PAGE_NEW, Crud::PAGE_INDEX, Crud::PAGE_DETAIL])) {
            $choices = [];
            foreach ($this->nodeTypeService->getNodeTypes() as $class) {
                $choices[$class::getName()] = $class;
            }

            yield ChoiceField::new('type')->setChoices($choices);

        } elseif (Crud::PAGE_EDIT === $pageName) {

            $node = $this->getContext()->getEntity()->getInstance();

            if (!($node->getTypeObject($this->nodeTypeService) instanceof TriggerableNodeInterface)) {
                yield from $this->getParentFields($node);
            }

            yield from $node->getTypeObject($this->nodeTypeService)->configureFields($node);
        }

        return parent::configureFields($pageName);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Node $entityInstance
     * @return void
     */
    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $workflow = $this->getRepository(Workflow::class)->find($this->workflow_id);
        $entityInstance->setWorkflow($workflow);

        $nodeType = $entityInstance->getTypeObject($this->nodeTypeService);

        $entityInstance->setTriggerable($nodeType instanceof TriggerableNodeInterface);

        parent::persistEntity($entityManager, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setTriggerable($entityInstance->getTypeObject($this->nodeTypeService) instanceof TriggerableNodeInterface);

        parent::updateEntity($entityManager, $entityInstance);
    }

    private function getParentFields(Node $node)
    {
        $repository = $this->getRepository(self::getEntityFqcn());
        $queryBuilder = $repository->createQueryBuilder('node');
        $queryBuilder->orderBy('node.name', 'ASC');
        $queryBuilder->where("node.id != {$node->getId()}");
        $queryBuilder->andWhere("node.workflow_id = {$node->getWorkflowId()}");

        $parentChoices = [];
        foreach ($queryBuilder->getQuery()->getResult() as $nodeParent) {
            $parentChoices[$nodeParent->getName()] = $nodeParent->getId();
        }
        yield ChoiceField::new('parent_id', 'Parent')->setChoices($parentChoices);
        yield TextField::new('parent_status_result');
    }

    public function createEntity(string $entityFqcn) {
        $node = new Node();
        $node->setWorkflowId($this->workflow_id);
        return $node;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('name');
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $queryBuilder = parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $queryBuilder->andWhere('entity.workflow_id = :param_workflow')
            ->setParameter('param_workflow', $this->workflow_id);

        return $queryBuilder;
    }
}
