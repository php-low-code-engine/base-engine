<?php

namespace App\Validator\Constraints;

use App\Entity\Node;
use App\Entity\Workflow;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[\Attribute]
class MoreOneTriggerWorkflow extends \Symfony\Component\Validator\Constraint implements ConstraintValidatorInterface
{
    /**
     * @var ExecutionContextInterface
     */
    protected $context;

    /**
     * @return void
     */
    public function initialize(ExecutionContextInterface $context)
    {
        $this->context = $context;
    }

    public function validatedBy():string
    {
        return self::class;
    }

    /**
     * @param Workflow $workflow
     * @param Constraint $constraint
     * @return void
     */
    public function validate(mixed $workflow, Constraint $constraint):void
    {
        $triggerableNodes = $workflow->getNodes()->filter(function (Node $node) {
            return $node->getTriggerable();
        });

        if ($triggerableNodes->count() > 1) {
            $this->context->buildViolation('Workflow can not be contains > 1 triggerable node')
                ->addViolation();
        }
    }
}
