<?php

namespace App\Validator\Constraints;

use App\Entity\Intergration;
use App\Services\Provider\ProviderService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[\Attribute]
class ProviderCreds extends \Symfony\Component\Validator\Constraint implements ConstraintValidatorInterface
{
    /**
     * @var ExecutionContextInterface
     */
    protected $context;

    private ProviderService $providerService;

    /**
     * @return void
     */
    public function initialize(ExecutionContextInterface $context)
    {
        $this->context = $context;
    }

    public function validatedBy():string
    {
        return self::class;
    }

    public function setProviderService(ProviderService $providerService)
    {
        $this->providerService = $providerService;
    }

    /**
     * @param array $params
     * @param ProviderCreds $constraint
     * @return void
     */
    public function validate(mixed $params, Constraint $constraint):void
    {
        /** @var Intergration $integration */
        $integration = $this->context->getObject();
        $integration->getProvider($this->providerService)->validateCreds($params ?? [], $constraint, $this->context);
    }
}
