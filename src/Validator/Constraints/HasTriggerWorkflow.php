<?php

namespace App\Validator\Constraints;

use App\Entity\Node;
use App\Services\Trigger\TriggerableNodeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[\Attribute]
class HasTriggerWorkflow extends \Symfony\Component\Validator\Constraint implements ConstraintValidatorInterface
{
    /**
     * @var ExecutionContextInterface
     */
    protected $context;

    /**
     * @return void
     */
    public function initialize(ExecutionContextInterface $context)
    {
        $this->context = $context;
    }

    public function validatedBy():string
    {
        return self::class;
    }

    /**
     * @param ArrayCollection $nodes
     * @param Constraint $constraint
     * @return void
     */
    public function validate(mixed $nodes, Constraint $constraint):void
    {
        if (!$nodes) {
            return;
        }

        $triggerableNodes = $nodes->filter(function (Node $node) {
            return $node->getTriggerable();
        });

        if ($triggerableNodes->isEmpty()) {
            $this->context->buildViolation('Workflow must be contains triggerable node')
                ->addViolation();
        }

        if ($triggerableNodes->count() > 1) {
            $this->context->buildViolation('Workflow can not be contains > 1 triggerable node')
                ->addViolation();
        }
    }
}
