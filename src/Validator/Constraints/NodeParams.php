<?php

namespace App\Validator\Constraints;

use App\Entity\Node;
use App\Services\NodeType\NodeTypeFactoryService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[\Attribute]
class NodeParams  implements ConstraintValidatorInterface
{
    /**
     * @var ExecutionContextInterface
     */
    protected $context;

    public function __construct(
        private readonly NodeTypeFactoryService $nodeTypeFactoryService
    )
    {
    }

    /**
     * @return void
     */
    public function initialize(ExecutionContextInterface $context)
    {
        $this->context = $context;
    }

    public function validatedBy():string
    {
        return self::class;
    }

    /**
     * @param array $params
     * @param NodeParams $constraint
     * @return void
     */
    public function validate(mixed $params, Constraint $constraint):void
    {
        /** @var Node $node */
        $node = $this->context->getObject();
        $node->getTypeObject($this->nodeTypeFactoryService)->validateParams($params, $constraint, $this->context);
    }
}
