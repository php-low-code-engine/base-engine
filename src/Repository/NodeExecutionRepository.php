<?php

namespace App\Repository;

use App\Entity\NodeExecution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<NodeExecution>
 *
 * @method NodeExecution|null find($id, $lockMode = null, $lockVersion = null)
 * @method NodeExecution|null findOneBy(array $criteria, array $orderBy = null)
 * @method NodeExecution[]    findAll()
 * @method NodeExecution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NodeExecutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NodeExecution::class);
    }

}
