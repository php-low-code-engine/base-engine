<?php

namespace App\Repository;

use App\Entity\Intergration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Intergration>
 *
 * @method Intergration|null find($id, $lockMode = null, $lockVersion = null)
 * @method Intergration|null findOneBy(array $criteria, array $orderBy = null)
 * @method Intergration[]    findAll()
 * @method Intergration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IntergrationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Intergration::class);
    }

//    /**
//     * @return Intergration[] Returns an array of Intergration objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Intergration
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
