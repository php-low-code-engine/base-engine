<?php

namespace App\Repository;

use App\Entity\Node;
use App\Entity\Workflow;
use App\Services\Execution\NodesCollection;
use App\Services\Trigger\Event\WorkflowCangeStateEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * @extends ServiceEntityRepository<Node>
 *
 * @method Node|null find($id, $lockMode = null, $lockVersion = null)
 * @method Node|null findOneBy(array $criteria, array $orderBy = null)
 * @method Node[]    findAll()
 * @method Node[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Node    getOneOrNullResult()
 */
class NodeRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private readonly CacheInterface $cache,
        private readonly LoggerInterface $logger
    )
    {
        parent::__construct($registry, Node::class);
    }

    #[AsEventListener(WorkflowCangeStateEvent::class, priority: 256)]
    public function onBeforeChangeWorkflow(WorkflowCangeStateEvent $workflowCangeStateEvent): void
    {
        $workflow = $workflowCangeStateEvent->getWorkflow();

        $this->logger->info("Clear cache", [
            'workflow' => $workflow->getId(), 'method' => __METHOD__]);

        // TODO Clear dont work
        $this->cache->delete("trigerable_nodes_{$workflow->getId()}");
        $this->cache->delete("nodes_{$workflow->getId()}");
        $this->cache->delete("all_trigerable_nodes");
        $this->cache->delete("all_active_trigerable_nodes");
    }

    public function getWorkflowTriggerableNode(Workflow $workflow): ?Node
    {
        $cacheKey = "trigerable_nodes_{$workflow->getId()}";
        $builder = $this->createQueryBuilder('n')
            ->where("n.workflow_id = '{$workflow->getId()}'")
            ->andWhere("n.triggerable = true");

        $query = $builder->getQuery();
        $query->enableResultCache(60*60*24, $cacheKey);

        return $query->getOneOrNullResult();
    }

    /**
     * @return Node[]
     */
    public function getTriggerableNodes():array
    {
        $cacheKey = "all_trigerable_nodes";
        $builder = $this->createQueryBuilder('n')
            ->andWhere("node.triggerable = true");

        $query = $builder->getQuery();
        $query->enableResultCache(60*60*24, $cacheKey);

        return $query->getResult();
    }

    /**
     * @return Node[]
     */
    public function getActiveTriggerableNodes():array
    {
        $cacheKey = "all_active_trigerable_nodes";

        $builder = $this->createQueryBuilder('node')
            ->innerJoin(Workflow::class, 'w', Join::WITH, 'node.workflow_id = w.id')
            ->andWhere('w.active = true')
            ->andWhere("node.triggerable = true");

        $query = $builder->getQuery();
        $query->enableResultCache(60*60*24, $cacheKey);

        return $query->getResult();
    }

    public function getNodesByWorkflowId($workflow_id):NodesCollection
    {
        $cacheKey = "nodes_{$workflow_id}";
        $builder = $this->createQueryBuilder('n')
            ->andWhere('n.workflow_id = :workflow_id')
            ->setParameters([':workflow_id' => $workflow_id]);

        $query = $builder->getQuery();

        $query->enableResultCache(60*60*24, $cacheKey);

        return new NodesCollection($query->getResult());
    }

    public function getTriggerableNodesByFields($fields)
    {
        if (!$fields['triggerable'] ?? false) {
            return [];
        }

        return $this->createQueryBuilder('n')
            ->andWhere('n.workflow_id = :workflow_id')
            ->andWhere('n.triggerable = true')
            ->setParameters([':workflow_id' => $fields['workflow_id']])
            ->getQuery()->getResult();
    }
}
