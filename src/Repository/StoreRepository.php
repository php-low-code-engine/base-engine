<?php

namespace App\Repository;

use App\Entity\StoreItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StoreItem>
 *
 * @method StoreItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method StoreItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method StoreItem[]    findAll()
 * @method StoreItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StoreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StoreItem::class);
    }

    public function existsInTable($id):bool
    {
        return $this->createQueryBuilder('si')
                ->select('COUNT(si.id)')
                ->where("si.id = :id")
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleScalarResult() > 0;
    }

    public function delete($key):bool
    {
        return (bool) $this->createQueryBuilder('si')
            ->delete(StoreItem::class, 'si')
            ->where("si.id = :id")
            ->setParameter('id', $key)
            ->getQuery()->execute();
    }
}
