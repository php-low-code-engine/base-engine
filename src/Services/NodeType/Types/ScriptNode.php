<?php

namespace App\Services\NodeType\Types;

use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Services\Execution\ExecutionNodeInterface;
use App\Validator\Constraints\NodeParams;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ScriptNode implements NodeTypeInterface, ExecutionNodeInterface
{

    public function __construct(
        private readonly LoggerInterface $logger
    )
    {
    }

    public function configureFields(Node $node): iterable
    {
        yield CodeEditorField::new('script')->setLabel('Script')->setValue($node->getParams()['name'] ?? null);
    }

    static public function getName(): string
    {
        return 'Скрипт';
    }

    public function getFieldsKeys():array
    {
        $entity = new Node();
        $fields = $this->configureFields($entity);

        $keys = [];
        foreach ($fields as $field) {
            $keys[] = $field->getAsDto()->getProperty();
        }

        return $keys;
    }

    public function set(Node $entity, $key, $value)
    {
        if (!in_array($key, $this->getFieldsKeys())) {
            return;
        }

        $creds = $entity->getParams();
        $creds[$key] = $value;
        $entity->setParams($creds);
    }

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context)
    {
        // TODO: Implement validate() method.
    }

    public function run(NodeExecution $nodeExecution): string
    {
        try {
            $script = $nodeExecution->getNode()->getParam('script');
            return eval($script);
        } catch (\Throwable $tr) {
            $this->logger->error('', ['ex' => $tr, 'method' => __METHOD__]);
            return ExecutionNodeInterface::ERROR_RESULT;
        }

    }
}
