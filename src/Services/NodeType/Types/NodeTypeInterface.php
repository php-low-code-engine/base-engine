<?php

namespace App\Services\NodeType\Types;

use App\Entity\Node;
use App\Validator\Constraints\NodeParams;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

interface NodeTypeInterface
{
    static public function getName():string;

    public function configureFields(Node $node):iterable;

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context);
}

