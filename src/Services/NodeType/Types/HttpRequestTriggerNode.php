<?php

namespace App\Services\NodeType\Types;

use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Services\Execution\ExecutionNodeInterface;
use App\Services\Execution\ExecutionService;
use App\Services\Trigger\Event\HttpRequestTriggerEvent;
use App\Services\Trigger\TriggerableNodeInterface;
use App\Validator\Constraints\NodeParams;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class HttpRequestTriggerNode implements NodeTypeInterface, TriggerableNodeInterface, ExecutionNodeInterface
{

    public function __construct(
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly LoggerInterface $logger,
        private readonly ExecutionService $executionService
    )
    {
    }

    public function configureFields(Node $node): iterable
    {
        yield TextField::new('url', 'Url')->setValue($node->getParam('url'));
    }

    static public function getName(): string
    {
        return 'HttpRequestTrigger';
    }

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context)
    {
        // TODO: Implement validate() method.
    }

    public function addTrigger(Node $node, string $type)
    {
        $this->eventDispatcher->addListener('http.request.event', $this->getCallable($node));
    }

    public function getCallable(Node $node): \Closure
    {
        return function (HttpRequestTriggerEvent $event) use ($node) {
            $settingsUrl = $node->getParam('url');
            $dataRequest = array_merge(
                $event->getRequest()->request->all(),
                $event->getRequest()->query->all()
            );
            if ($settingsUrl === $event->getUrl()) {
                $this->logger->info("Triggered node '{$node->getId()}'", [
                    'method' => __METHOD__,
                    'request' => $dataRequest
                ]);

                $execution = $this->executionService->createExecution($node->getWorkflow(), $node, $dataRequest);
                $this->executionService->runExecutionAsync($execution);

                $event->setResponse(new Response("Execution node '{$node->getId()}' started"));
            }
        };
    }

    public function removeTrigger(Node $node, string $type)
    {
        $this->eventDispatcher->removeListener('http.request.event', $this->getCallable($node));
    }

    public function run(NodeExecution $nodeExecution):string
    {
        return ExecutionNodeInterface::DEFAULT_RESULT;
    }

}
