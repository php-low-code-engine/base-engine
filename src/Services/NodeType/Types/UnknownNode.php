<?php

namespace App\Services\NodeType\Types;

use App\Entity\Node;
use App\Validator\Constraints\NodeParams;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class UnknownNode implements NodeTypeInterface
{

    static public function getName(): string
    {
        return 'Unknown node';
    }

    public function configureFields(Node $node): iterable
    {
        return [];
    }

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context)
    {
        // TODO: Implement validateParams() method.
    }
}
