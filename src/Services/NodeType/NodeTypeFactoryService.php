<?php

namespace App\Services\NodeType;

use App\Services\NodeType\Types\NodeTypeInterface;
use App\Services\NodeType\Types\UnknownNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NodeTypeFactoryService
{


    public function __construct(
        private readonly ContainerInterface $container
    )
    {
    }

    public function getNodeTypes(string $interfaceName = NodeTypeInterface::class): array
    {
        $implementingServices = [];
        $serviceIds = $this->container->getServiceIds();

        foreach ($serviceIds as $serviceId) {
            $service = $this->container->get($serviceId);
            if ($service instanceof $interfaceName) {
                $implementingServices[] = $serviceId;
            }
        }

        return $implementingServices;
    }

    /**
     * @param $classType
     * @return object
     */
    public function getNodeType($classType): NodeTypeInterface
    {
        if (!$this->container->has($classType)) {
            return $this->container->get(UnknownNode::class);
        }
        return $this->container->get($classType);
    }
}
