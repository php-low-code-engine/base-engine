<?php

namespace App\Services\Template;

use App\Services\Execution\ExecutionData;

interface TemplateServiceInterface
{
    public function render(string $template, ExecutionData $executionData) : string;
}
