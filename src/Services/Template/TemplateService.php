<?php

namespace App\Services\Template;

use App\Entity\NodeExecution;
use App\Services\Execution\ExecutionData;
use App\Services\Execution\ExecutionException;
use Psr\Log\LoggerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class TemplateService implements TemplateServiceInterface
{
    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    public function render(string $template, ExecutionData $executionData) : string
    {
        $map = [];
        preg_match_all('/{{\s?([\w.]+)\s?}}/', $template, $map);

        $replace = [];
        foreach ($map[1] as $key) {
            $replace[] = $this->getValue($executionData, $key);
        }

        $result = str_replace($map[0], $replace, $template);

        $this->logger->info('Render template', [
            'template' => $template,
            'data' => $executionData->toArray(),
            'result' => $result,
            'method' => __METHOD__
        ]);

        return $result;
    }

    private function getValue(ExecutionData $executionData, $dotkey)
    {
        $path = explode('.', $dotkey) ?? [];

        if (current($path) === 'execution') {
            $executionData = $this->getExecutionData($executionData, $path);
        }

        return $this->getValueByPath($executionData, $path);
    }

    private function getExecutionData(ExecutionData $executionData, array &$path): ExecutionData
    {
        $nodeExecutions = $executionData->getNodeExecution()->getExecution()->getNodeExecutions();

        if (!$nodeId = ($path[1] ?? false) or !is_numeric($nodeId)) {
            $dotPath = join('.', $path);
            throw new ExecutionException("Wrong path to node in template '{{{$dotPath}}}'");
        }

        if (!$dataType = ($path[2] ?? false) or !in_array($dataType, ['input', 'output'])) {
            $dotPath = join('.', $path);
            throw new ExecutionException("Wrong path to data type in template '{{{$dotPath}}}'");
        }

        /** @var NodeExecution $nodeExecution */
        $nodeExecution = $nodeExecutions->findFirst(function ($key, NodeExecution $nodeExecution) use ($nodeId) {
            return $nodeExecution->getNodeId() === intval($nodeId);
        });

        if (!$nodeExecution) {
            throw new ExecutionException("Not found node by template '{{{$nodeId}}}'");
        }

        $path = array_slice($path, 3);

        if ($dataType === 'input') {
            return $nodeExecution->getInputData();
        } else {
            return $nodeExecution->getOutputData();
        }

    }

    private function getValueByPath(ExecutionData $executionData, array $path)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->disableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();

        $pathAccessor = [];
        foreach ($path as $item) {
            $pathAccessor[] = "[{$item}]";
        }

        return $propertyAccessor->getValue($executionData->toArray(), join('', $pathAccessor));
    }

}
