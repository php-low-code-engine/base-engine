<?php

namespace App\Services\Execution;

use App\Entity\Node;

class NodesCollection extends \Doctrine\Common\Collections\ArrayCollection
{
    public function findNextNode($parent_id, $parent_status_result): Node|bool
    {
        $nextNodes = $this->filter(function (Node $node) use ($parent_id, $parent_status_result) {
            return $node->getParentId() === $parent_id and $node->getParentStatusresult() === $parent_status_result;
        });

        /** @var Node $nextNode */
        $nextNode = $nextNodes->first();
        return $nextNode;
    }
}
