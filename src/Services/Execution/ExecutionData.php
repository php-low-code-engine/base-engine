<?php

namespace App\Services\Execution;

use App\Entity\NodeExecution;
use Doctrine\Common\Collections\ArrayCollection;

class ExecutionData extends ArrayCollection
{
    public function __construct(array $elements, private readonly ?NodeExecution $nodeExecution = null)
    {
        parent::__construct($elements);
    }

    /**
     * @return NodeExecution
     */
    public function getNodeExecution(): NodeExecution
    {
        return $this->nodeExecution;
    }

}
