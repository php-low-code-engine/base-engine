<?php

namespace App\Services\Execution;

use App\Entity\Execution;
use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Entity\User;
use App\Entity\Workflow;
use App\Services\Execution\Messenger\Message\ExecutionMessage;
use App\Services\NodeType\NodeTypeFactoryService;
use App\Services\Security\AppSecurity;
use App\Services\Trigger\Event\ExecutionFinishedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Service\ResetInterface;

class ExecutionService implements ResetInterface
{
    private ?NodesCollection $executionNodes = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly AppSecurity $appSecurity,
        private readonly LoggerInterface $logger,
        private readonly SerializerInterface $serializer,
        private readonly NodeTypeFactoryService $nodeTypeService,
        private readonly MessageBusInterface $messageBus,
        private readonly EventDispatcherInterface $eventDispatcher
    )
    {
    }

    public function createExecution(
        /*remove*/ Workflow $workflow,
                   Node $node,
                   array $data
    ): Execution
    {
        $workflow = $node->getWorkflow();

        $this->logger->info('Create execution', [
            'workflow_id' => $workflow->getId(),
            'node_id' => $node->getId(),
            'data' => $data,
            'method'=> __METHOD__
        ]);

        $user_id = $this->appSecurity->getSystemUser()->getId();
        $user = $this->entityManager->find(User::class, $user_id);

        $execution = new Execution();
        $execution->setUser($user);
        $execution->setWorkflow($workflow);
        !empty($data['session_id']) && $execution->setSessionId($data['session_id']);
        $this->entityManager->persist($execution);

        $nodeExecution = new NodeExecution($node, $data);
        $nodeExecution->setExecution($execution);
        $nodeExecution->setExecuteOrder(1);
        $execution->getNodeExecutions()->add($nodeExecution);
        $this->entityManager->persist($nodeExecution);

        $this->entityManager->flush();

        return $execution;
    }

    public function runExecutionAsync(Execution $execution)
    {
        $this->messageBus->dispatch(new ExecutionMessage($execution->getId()));
    }

    public function runExecution(Execution $execution): Execution
    {
        $execution->setStarted(new \DateTime());
        $this->logger->info('Run execution', [
            'execution' => $this->serializer->normalize($execution, 'json', ['groups' => 'log']),
            'method'=> __METHOD__
        ]);

        /** @var NodeExecution $nodeExecution */
        $nodeExecution = $execution->getNodeExecutions()->last();

        $this->runNodeExecutionRecursive($nodeExecution);

        $execution->setFinished(new \DateTime());
        $execution->setStatus($nodeExecution->getStatusResult());
        $this->logger->info('Finished execution', [
            'execution' => $this->serializer->normalize($execution, 'json', ['groups' => 'log']),
            'method'=> __METHOD__]);

        $this->entityManager->persist($execution);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(new ExecutionFinishedEvent($execution));

        return $execution;
    }

    public function runNodeExecutionRecursive(NodeExecution $nodeExecution)
    {
        $execution = $nodeExecution->getExecution();

        if ($this->checkLooping($execution, $nodeExecution)) {
            $loopingNode = $nodeExecution->getNode();
            $this->logger->warning('Detect looping node', [
                'node' => $this->serializer->normalize($loopingNode, 'json', ['groups' => 'log']),
                'method' => __METHOD__
            ]);
        }

        $status_result = $this->runNodeExecution($nodeExecution);

        if ($status_result === ExecutionNodeInterface::ERROR_RESULT) {
            return;
        }

        $nextNode = $this->findNextNode(
            $execution->getWorkflowId(),
            $nodeExecution->getNode()->getId(),
            $status_result
        );

        if (!$nextNode) {
            $this->logger->info('Not founded next node', [
                'workflow_id' => $execution->getWorkflowId(),
                'parent_node_id' => $nodeExecution->getNode()->getId(),
                'status_result' => $status_result,
                'method' => __METHOD__
            ]);
            return;
        }

        $nextNodeExecution = new NodeExecution($nextNode, $nodeExecution->getOutputData()->toArray());
        $nextNodeExecution->setExecution($execution);
        $this->entityManager->persist($nextNodeExecution);
        $nextNodeExecution->setExecuteOrder($nodeExecution->getExecuteOrder() + 1);
        $execution->getNodeExecutions()->add($nextNodeExecution);
        $this->entityManager->flush();

        $this->runNodeExecutionRecursive($nextNodeExecution);
    }

    private function findNextNode($workflow_id, $parent_node_id, $status_result)
    {
        $executionNodes = $this->getExecutionNodesByWorkflowId($workflow_id);
        $nextNode = $executionNodes->findNextNode($parent_node_id, $status_result);

        if ($nextNode) {
            $this->logger->info('Founded next node', [
                'node' => $this->serializer->normalize($nextNode, 'json', ['groups' => 'log']),
                'method' => __METHOD__
            ]);
        }

        return $nextNode;
    }

    public function checkLooping(Execution $execution, NodeExecution $nodeExecution):bool
    {
        $finishedNodeExecutions = $execution->getNodeExecutions()->filter(function (NodeExecution $nodeExecution) {
            return $nodeExecution->getFinished();
        });

        $loopingNodeExecutions = $finishedNodeExecutions->filter(function (NodeExecution $finishedNodeExecution) use ($nodeExecution) {
            return $finishedNodeExecution->getNode()->getId() === $nodeExecution->getNode()->getId();
        });

        if (!$loopingNodeExecutions->isEmpty()) {
            if ($execution->getWorkflow()->avalable_loop_repeat < $loopingNodeExecutions->count()) {
                throw new ExecutionException("Too many repeat looping for node '{$nodeExecution->getNode()->getId()}'");
            }

            return true;
        }

        return false;
    }

    private function getExecutionNodesByWorkflowId(int $workflow_id):NodesCollection
    {
        if (!$this->executionNodes) {
            $nodeRepository = $this->entityManager->getRepository(Node::class);
            $this->executionNodes = $nodeRepository->getNodesByWorkflowId($workflow_id);
        }

        return $this->executionNodes;
    }

    public function runNodeExecution(NodeExecution $nodeExecution): string
    {
        if (!$node = $nodeExecution->getNode()) {
            throw new ExecutionException("Not found node by nodeExecution '{$nodeExecution->getId()}'");
        }
        $nodeType = $node->getTypeObject($this->nodeTypeService);

        if (!$nodeType instanceof ExecutionNodeInterface) {
            throw new ExecutionException("Node '{$node->getId()}' can not be run, node type is" . get_class($nodeType));
        }

        $nodeExecution->setStarted(new \DateTime());

        $this->logger->info('Run node', [
            'node' => $this->serializer->normalize($node, 'json', ['groups' => 'log']),
            'nodeExecution' => $this->serializer->normalize($nodeExecution, 'json', ['groups' => 'log']),
            'method'=> __METHOD__
        ]);

        try {
            $status_result = $nodeType->run($nodeExecution);
        } catch (\Throwable $tr) {
            $this->logger->error($tr->getMessage(), ['exception' => $tr, 'method'=> __METHOD__]);
            $status_result = ExecutionNodeInterface::ERROR_RESULT;
        }

        $this->logger->info(
            'Node execution finished',
            [
                'node_id' => $node->getId(),
                'node_execution_id' => $nodeExecution->getId(),
                'output_data' => $nodeExecution->getOutputData()->toArray(),
                'status_result' => $status_result,
                'method'=> __METHOD__
            ]
        );

        $nodeExecution->setStatusResult($status_result);
        $nodeExecution->setFinished(new \DateTime());
        $this->entityManager->persist($nodeExecution);
        $this->entityManager->flush();

        return $status_result;
    }

    public function reset():void
    {
        $this->logger->info("Clear ExecutionService for proccess: " . getmypid());
        $this->entityManager->clear();
        $this->executionNodes = null;
    }
}
