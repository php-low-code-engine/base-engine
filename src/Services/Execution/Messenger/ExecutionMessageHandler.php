<?php

namespace App\Services\Execution\Messenger;

use App\Entity\Execution;
use App\Services\Execution\ExecutionException;
use App\Services\Execution\ExecutionService;
use App\Services\Execution\Messenger\Message\ExecutionMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class ExecutionMessageHandler
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ExecutionService $executionService
    )
    {
    }

    public function __invoke(ExecutionMessage $message): void
    {
        $execution_id = $message->getExecutionId();
        /** @var Execution $execution */
        if (!$execution = $this->entityManager->getRepository(Execution::class)->find($execution_id)) {
            throw new ExecutionException("Not found execution_id by id '{$execution_id}'");
        }

        $this->executionService->runExecution($execution);
    }
}
