<?php

namespace App\Services\Execution\Messenger\Message;

class ExecutionMessage
{
    public function __construct(private readonly string $execution_id)
    {
    }

    /**
     * @return string
     */
    public function getExecutionId(): string
    {
        return $this->execution_id;
    }

}
