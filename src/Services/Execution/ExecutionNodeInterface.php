<?php

namespace App\Services\Execution;

use App\Entity\NodeExecution;

interface ExecutionNodeInterface
{
    const ERROR_RESULT = '1';
    const DEFAULT_RESULT = '2';

    public function run(NodeExecution $nodeExecution): string;
}
