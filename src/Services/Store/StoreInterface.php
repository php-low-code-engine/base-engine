<?php

namespace App\Services\Store;

interface StoreInterface
{
    public function get(string $key);

    public function set(string $key, $value);

    public function has(string $key):bool;

    public function remove(string $key):bool;
}
