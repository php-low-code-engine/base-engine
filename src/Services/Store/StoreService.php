<?php

namespace App\Services\Store;

use App\Entity\StoreItem;
use Doctrine\ORM\EntityManagerInterface;

class StoreService implements StoreInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    )
    {
    }

    public function get(string $key)
    {
        if (!$item = $this->entityManager->find(StoreItem::class, $key)) {
            return null;
        }

        $value = $item->getValue();

        if ($item->getType() === 'json') {
            $value = json_decode($value, true);
        }

        return $value;
    }

    public function set(string $key, $value)
    {
        if ($this->has($key)) {
            $this->remove($key);
        }

        $item = new StoreItem();
        $item->setId($key);
        $item->setType('string');
        $item->setCreated(new \DateTime('now'));

        if (is_array($value)) {
            $item->setType('json');
            $value = json_encode($value, JSON_UNESCAPED_UNICODE);
        }
        $item->setValue($value);

        $this->entityManager->persist($item);
        $this->entityManager->flush();
        $this->entityManager->detach($item);
    }

    public function remove(string $key):bool
    {
        return $this->entityManager->getRepository(StoreItem::class)
            ->delete($key);
    }

    public function has(string $key):bool
    {
        return $this->entityManager->getRepository(StoreItem::class)
            ->existsInTable($key);
    }
}
