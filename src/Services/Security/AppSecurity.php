<?php

namespace App\Services\Security;

use App\Entity\User;
use App\Repository\UserRepository;

/**
 * @method User getUser()
 */
class AppSecurity
{

    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    public function getSystemUser():User
    {
        $builder = $this->userRepository->createQueryBuilder('u');
        $builder->where('u.is_system = true');
        if (!$user = $builder->getQuery()->getOneOrNullResult()) {
            throw new \ErrorException('Not found system user. Try php bin/console app:create-admin');
        }

        return $user;
    }
}
