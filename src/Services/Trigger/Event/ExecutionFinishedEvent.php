<?php

namespace App\Services\Trigger\Event;

use App\Entity\Execution;

class ExecutionFinishedEvent
{
    const NAME = 'execution.finished';

    public function __construct(private readonly Execution $execution)
    {
    }

    /**
     * @return Execution
     */
    public function getExecution(): Execution
    {
        return $this->execution;
    }
}
