<?php

namespace App\Services\Trigger\Event;

use App\Entity\Workflow;

class WorkflowCangeStateEvent
{
    public function __construct(
        private readonly Workflow $workflow,
        private readonly string $triggerType
    )
    {
    }

    /**
     * @return Workflow
     */
    public function getWorkflow(): Workflow
    {
        return $this->workflow;
    }

    /**
     * @return string
     */
    public function getTriggerType(): string
    {
        return $this->triggerType;
    }
}
