<?php

namespace App\Services\Trigger;

use App\Entity\Node;

interface TriggerableNodeInterface
{
    public function addTrigger(Node $node, string $type);

    public function removeTrigger(Node $node, string $type);
}
