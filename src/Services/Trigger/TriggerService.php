<?php

namespace App\Services\Trigger;

use ApiPlatform\Validator\ValidatorInterface;
use App\Entity\Node;
use App\Entity\Workflow;
use App\Repository\NodeRepository;
use App\Services\NodeType\NodeTypeFactoryService;
use App\Services\Trigger\Event\WorkflowCangeStateEvent;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Event\AbstractLifecycleEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityDeletedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\EventDispatcher\Event;

class TriggerService
{
    const TYPE_SYSTEM = 'system';
    const TYPE_REMOTE = 'remote';
    const TYPE_ALL = 'all';

    private bool $activated = false;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly NodeRepository $nodeRepository,
        private readonly LoggerInterface $logger,
        private readonly NodeTypeFactoryService $nodeTypeService,
        private readonly EventDispatcherInterface $eventDispatcher
    )
    {
    }

    #[AsEventListener(AfterEntityUpdatedEvent::class, priority: 256)]
    #[AsEventListener(AfterEntityDeletedEvent::class, priority: 256)]
    #[AsEventListener(AfterEntityPersistedEvent::class, priority: 256)]
    public function onChangeEntity(AbstractLifecycleEvent $lifecycleEvent): void
    {
        $entity = $lifecycleEvent->getEntityInstance();

        if ($entity instanceof Workflow) {
            if ($entity->isActive()) {
                $this->activateWorkflow($entity, self::TYPE_REMOTE);
            } else {
                $this->deactivateWorkflow($entity, self::TYPE_REMOTE);
            }
        } elseif ($entity instanceof Node) {
            $this->nodeUpdated($entity);
        }
    }

    #[AsEventListener(BeforeEntityDeletedEvent::class)]
    public function onBeforeEntityDeleted(BeforeEntityDeletedEvent $beforeEntityDeletedEvent): void
    {
        $entity = $beforeEntityDeletedEvent->getEntityInstance();

        if ($entity instanceof Workflow) {
            $this->deactivateWorkflow($entity, self::TYPE_ALL);
        }

        if ($entity instanceof Node) {
            $this->nodeUpdated($entity);
        }
    }

    #[AsEventListener(event: KernelEvents::REQUEST, priority: 256)]
    public function activate(Event $event): void
    {
        if ($this->activated) {
            return;
        }

        $this->activated = true;

        $this->logger->debug('Start activate triggers', ['method' => __METHOD__]);

        $triggerableNodes = $this->nodeRepository->getActiveTriggerableNodes();

        foreach ($triggerableNodes as $triggerableNode) {
            $this->addNodeTrigger($triggerableNode, self::TYPE_SYSTEM);
        }
    }

    private function addNodeTrigger(Node $node, string $type): void
    {
        $this->logger->debug("Activate trigger", ['name' => $node->getName(), 'type' => $type, 'method' => __METHOD__]);
        $node->getTypeObject($this->nodeTypeService)->addTrigger($node, $type);
    }

    private function removeNodeTrigger(Node $node, string $type): void
    {
        $this->logger->debug("Remove trigger", ['name' => $node->getName(), 'type' => $type, 'method' => __METHOD__]);
        $node->getTypeObject($this->nodeTypeService)->removeTrigger($node, $type);
    }

    public function deactivate()
    {
        $this->logger->debug('Start deactivate triggers', ['method' => __METHOD__]);
        $triggerableNodes = $this->nodeRepository->getTriggerableNodes();

        foreach ($triggerableNodes as $triggerableNode) {
            $this->removeNodeTrigger($triggerableNode, self::TYPE_ALL);
        }
    }

    public function triggerNode(Node $node)
    {
        $this->logger->info("Triggered node '{$node->getName()}'", ['method' => __METHOD__]);

    }

    public function activateWorkflow(Workflow $workflow, string $triggerType): void
    {
        $this->logger->info("Activate workfow triggers", [
            'workflow' => $workflow->getId(), 'method' => __METHOD__]);

        $this->eventDispatcher->dispatch(new WorkflowCangeStateEvent($workflow, $triggerType));

        if (!$node = $this->nodeRepository->getWorkflowTriggerableNode($workflow)) {
            throw new TriggerException("Workflow '{$workflow->getName()}' not contains triggerable Nodes");
        }

        $this->addNodeTrigger($node, $triggerType);
    }

    public function deactivateWorkflow(Workflow $workflow, string $triggerType): void
    {
        $this->logger->info("Deactivate workfow triggers", [
            'workflow' => $workflow->getId(), 'method' => __METHOD__]);

        $this->eventDispatcher->dispatch(new WorkflowCangeStateEvent($workflow, $triggerType));

        if (!$node = $this->nodeRepository->getWorkflowTriggerableNode($workflow)) {
            $this->logger->warning("Workflow '{$workflow->getName()}' not contains triggerable Nodes", [
                'workflow' => $workflow->getId(), 'method' => __METHOD__]);
            return;
        }

        $this->removeNodeTrigger($node, $triggerType);

    }

    private function nodeUpdated(Node $node): void
    {
        $workflow = $node->getWorkflow();
        $workflow->setActive(false);
        $this->entityManager->persist($workflow);
        $this->entityManager->flush();

        $this->deactivateWorkflow($node->getWorkflow(), self::TYPE_ALL);
    }
}
