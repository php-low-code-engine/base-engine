<?php

namespace App\Services\Session;

use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionBagProxy;
use Symfony\Component\HttpFoundation\Session\Storage\MetadataBag;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;

class ExecutionSessionStorage implements SessionStorageInterface
{
    /**
     * @var false
     */
    private $started;
    /**
     * @var true
     */
    private $closed;

    /**
     * @var SessionBagInterface[]
     */
    private array $bags;

    public function __construct(
        private readonly \SessionHandlerInterface $saveHandler,
        private string $sessionName = 'EXECUTION_SESSION',
        private ?MetadataBag $metadataBag = null
    )
    {
    }

    /**
     * {@inheritdoc}
     */
    public function start() : bool {
        if ($this->started && !$this->closed) {
            return true;
        }

        $this->saveHandler->open('executions', $this->sessionName);

        $data = [];
        if ($serializedData = $this->saveHandler->read($this->session_id)) {
            $data = unserialize($serializedData);
        }

        $this->loadSession($data);

        return true;
    }

    /**
     * Load the session with attributes.
     *
     * After starting the session, PHP retrieves the session from whatever handlers
     * are set to (either PHP's internal, or a custom save handler set with session_set_save_handler()).
     * PHP takes the return value from the read() handler, unserializes it
     * and populates $_SESSION with the result automatically.
     *
     * @return void
     */
    protected function loadSession(?array &$session = null)
    {
        $this->getAttributesBag()->initialize($session);

        $this->started = true;
        $this->closed = false;
    }

    /**
     * {@inheritdoc}
     */
    public function regenerate($destroy = false, $lifetime = null) : bool {
        // .. ?
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function save() {

        $attributesBag = $this->getAttributesBag();
        $data = $attributesBag->all();
        if (!$this->saveHandler->write($this->session_id, serialize($data))) {
            throw new \ErrorException('Cant save session');
        }

        if (!$this->saveHandler->close()) {
            throw new \ErrorException('Cant save session');
        }

        $this->started = false;
        $this->closed = true;
    }

    private function getAttributesBag(): AttributeBagInterface
    {
        foreach ($this->bags as $bag) {
            if ($bag instanceof SessionBagProxy) {
                $bag = $bag->getBag();
            }
            if ($bag instanceof AttributeBagInterface) {
                return $bag;
            }
        }

        throw new \ErrorException('Not found AttributeBag in session');
    }

    public function isStarted(): bool
    {
        return $this->started;
    }

    public function getId(): string
    {
        return $this->session_id;
    }

    public function setId(string $id)
    {
        $this->session_id = $id;
    }

    public function getName(): string
    {
        return $this->sessionName;
    }

    public function setName(string $sessionName)
    {
        $this->sessionName = $sessionName;
    }

    /**
     * @return void
     */
    public function clear()
    {
        foreach ($this->bags as $bag) {
            $bag->clear();
        }

        $this->loadSession();
    }

    public function registerBag(SessionBagInterface $bag)
    {
        $this->bags[$bag->getName()] = $bag;
    }

    public function getBag(string $name): SessionBagInterface
    {
        if (!isset($this->bags[$name])) {
            throw new \InvalidArgumentException(sprintf('The SessionBagInterface "%s" is not registered.', $name));
        }

        if (!$this->started) {
            $this->start();
        }

        return $this->bags[$name];
    }

    /**
     * Gets the MetadataBag.
     */
    public function getMetadataBag(): MetadataBag
    {
        return $this->metadataBag;
    }
}
