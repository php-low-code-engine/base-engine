<?php

namespace App\Services\Session;

use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionFactoryInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Uid\UuidV6;

class ExecutionSessionFactory implements SessionFactoryInterface
{
    private $sessionName = 'EXECUTION_SESSION';

    public function __construct(
        private readonly \SessionHandlerInterface $handler,
        private readonly LoggerInterface $logger
    )
    {
    }

    public function createSession(RequestInterface $request = null): SessionInterface
    {
        $session_id = $this->getSessionId($request);

        $this->logger->info('Generate session', [
            'session_id' => $session_id,
            'method' => __METHOD__
        ]);

        $sessionStorage = new ExecutionSessionStorage($this->handler, $this->sessionName);
        $sessionStorage->setId($session_id);

        return new Session($sessionStorage);
    }

    private function getSessionId(RequestInterface $request): string
    {
        if ($request) {
            $cookies = $request->getHeader('Cookie');
            is_array($cookies) && $cookies = current($cookies);
            $cookieString = str_replace('; ', '&', $cookies);
            $cookieArray = [];
            parse_str($cookieString, $cookieArray);
            $session_id = $cookieArray[$this->sessionName] ?? null;
        }

        if (!$session_id) {
            $session_id = UuidV6::generate();
        }

        return $session_id;
    }
}
