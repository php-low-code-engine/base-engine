<?php

namespace App\Services\Provider;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ProviderService
{
    public function __construct(
        private readonly ContainerInterface $container
    )
    {
    }

    public function getProviders(string $interfaceName = ProviderInterface::class): array
    {
        $implementingServices = [];
        $serviceIds = $this->container->getServiceIds();

        foreach ($serviceIds as $serviceId) {
            $service = $this->container->get($serviceId);
            if ($service instanceof $interfaceName) {
                $implementingServices[] = $serviceId;
            }
        }

        return $implementingServices;
    }

    /**
     * @param $classType
     * @return object
     */
    public function getProvider($classType): ProviderInterface
    {
        return $this->container->get($classType);
    }
}
