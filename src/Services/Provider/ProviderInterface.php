<?php

namespace App\Services\Provider;

use App\Entity\Intergration;
use App\Validator\Constraints\ProviderCreds;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

interface ProviderInterface
{
    public function configureFields(Intergration $integration):iterable;

    static public function getName():string;

    public function validateCreds(array $creds, ProviderCreds $constraint, ExecutionContextInterface $context);
}
