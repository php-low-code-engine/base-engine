<?php

namespace App\Command;

use App\Entity\Execution;
use App\Services\Execution\ExecutionException;
use App\Services\Execution\ExecutionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StartExecution extends Command
{
    public function __construct(
        private readonly ExecutionService $executionService,
        private readonly EntityManagerInterface $entityManager
    )
    {
        parent::__construct('execution');
    }

    protected function configure(): void
    {
        $this
            ->setName('execution')
            ->setDescription('Start execution by node id')
            ->addArgument('execution_id', InputArgument::REQUIRED, 'Execution id');
    }

    public static function getDefaultName() :?string
    {
        return 'app:start-execution';
    }

    protected function execute(InputInterface $input, OutputInterface $output):int
    {
        $execution_id = $input->getArgument('execution_id');
        /** @var Execution $execution */
        if (!$execution = $this->entityManager->getRepository(Execution::class)->find($execution_id)) {
            throw new ExecutionException("Not found execution_id by id '{$execution_id}'");
        }

        $this->executionService->runExecution($execution);

        return 0;
    }
}
