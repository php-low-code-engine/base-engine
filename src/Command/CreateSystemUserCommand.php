<?php
// src/Command/CreateSuperadminCommand.php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSystemUserCommand extends Command
{
    private $entityManager;

    public static function getDefaultName() :?string
    {
        return 'app:create-system-user';
    }

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Creates a System User.')
            ->setHelp('This command creates a System User user for your application.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = new User();
        $user->setEmail($_ENV['SYSTEM_EMAIL']);
        // Set other properties for the default user if needed

        // Encode and set the password (assuming you're using Symfony's security system)
        $encodedPassword = password_hash($_ENV['SYSTEM_PASS'], PASSWORD_BCRYPT);
        $user->setPassword($encodedPassword);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setIsVerified(true);
        $user->setIsSystem(true);

        // Persist the user
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $output->writeln('System User created.');

        return Command::SUCCESS;
    }
}

