## PHP LOW CODE ENGINE

### Installation

Start containers
```shell
docker-compose up -d
```

Create  .env
```shell
# $(id -u)
USER_ID=1000
# $(id -g)
GROUP_ID=1000

APP_ENV=dev
APP_SECRET=s3e4HZLkQHKhQBJP
APP_DEBUG=true
XDEBUG_MODE=debug

DATABASE_URL=postgresql://bot_ai:qwas_12!@bot_ai_db/bot_ai?serverVersion=15&charset=utf8

###> nelmio/cors-bundle ###
CORS_ALLOW_ORIGIN='^https?://(localhost|127\.0\.0\.1)(:[0-9]+)?$'

###> symfony/mailer ###
MAILER_DSN=null://null

SYSTEM_EMAIL=example@gmail.com
SYSTEM_PASS=12456789

###> symfony/messenger ###
MESSENGER_TRANSPORT_DSN=amqp://guest:guest@plce-rabbitmq
###< symfony/messenger ###

```

Apply db schema 
```shell
docker-compose run --rm app php bin/console  doctrine:schema:update --force --complete
```

Create system user
```shell
docker-compose run --rm app php bin/console app:create-system-user
```

Run messenger daemon
```shell
docker-compose up -d daemons_app
```


Edit /etc/hosts
```shell
echo "127.0.0.1 php-low-code.loc" >> /etc/hosts
```

Go to http://php-low-code.loc/admin

### Extention development

Create your bundle in folder ./bundles with composer.json
```json
{
  "name": "your-bundle",
  "version": "0.0.1"
}
```
Attach it in project ```docker-compose run --rm composer composer require your-bundle```.

When bundle is ready to release you can move bundle to another repo
